package net.spacegeek224.NotificationLib;

public enum NotificationCode {
	CLOSED("@CLOSED"),
	ACLICKED("@ACTIONCLICKED");

	private final String text;

	/**
	 * @param text
	 */
	private NotificationCode(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
