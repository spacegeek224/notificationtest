package net.spacegeek224.NotificationLib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BasicNotification {
	private String title;
	private String msg;
	private String[] cmd;
	private String result;

	public BasicNotification(String title, String message) {
		this.setTitle(title);
		this.setMsg(message);
		this.cmd = new String[] { "/Users/marisusis/Downloads/alerter", "-message", message, "-title", title };
	}
	
	

	public void send() {
		result = "";
		try {
			Runtime r = Runtime.getRuntime();
			Process p = r.exec(cmd);

			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				// System.out.println(inputLine);
				result += inputLine;
			}
			in.close();

		} catch (IOException e) {
			// System.out.println(e);
		}
	}

	public String output() {
		return result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	protected void initCmd() {
		this.cmd = new String[] { "alerter", "-message", this.msg, "-title", this.title };
	}

}
